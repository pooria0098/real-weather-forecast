from django import forms
from .models import Email, Contact


class EmailForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email_address'].widget.attrs.update(
            {
                'placeholder': 'Enter your email to subscribe...',
                'type': 'text',
                'label': '',
            }
        )

    class Meta:
        model = Email
        fields = '__all__'
        widgets = {
            'email_address': forms.EmailInput(),
        }


class ContactForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['your_name'].widget.attrs.update(
            {
                'placeholder': 'Your name...',
                'type': 'text',
                'label': '',
            }
        )
        self.fields['your_email_address'].widget.attrs.update(
            {
                'placeholder': 'Email Address...',
                'type': 'text',
                'label': '',
            }
        )
        self.fields['your_company_name'].widget.attrs.update(
            {
                'placeholder': 'Company name..',
                'type': 'text',
                'label': '',
            }
        )
        self.fields['your_website'].widget.attrs.update(
            {
                'placeholder': 'Website...',
                'type': 'text',
                'label': '',
            }
        )
        self.fields['your_message'].widget.attrs.update(
            {
                'placeholder': 'Message...',
                'type': 'textarea',
                'label': '',
            }
        )

    class Meta:
        model = Contact
        fields = '__all__'
        widgets = {
            'email_address': forms.EmailInput(),
            'your_website': forms.URLInput(),
        }
