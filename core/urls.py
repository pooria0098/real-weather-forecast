from django.urls import path
from .views import video, contact, photos

app_name = 'core'
urlpatterns = [
    path('contact/', contact, name='contact'),
    path('video/<int:page_num>/', video, name='video'),
    path('photos/<int:page_num>/', photos, name='photos'),
]
