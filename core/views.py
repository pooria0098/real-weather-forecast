from django.core.paginator import Paginator
from django.shortcuts import render, redirect

from pypexels import PyPexels
from .forms import EmailForm, ContactForm
from pexels_api import API


def SubscriptionEmail(request):
    if request.method == 'POST':
        form = EmailForm(request.POST)
        if form.is_valid():
            form.save()
            redirect('forecast:index')
        else:
            raise ValueError('Validation failed')

    form = EmailForm()

    return render(request, 'base.html', {'email_form': form})


def photos(request, page_num):
    # print(request)
    # print(page_num)

    PEXELS_API_KEY = '563492ad6f917000010000016c169600b01549b190ba2ed39b1d9000'
    api = API(PEXELS_API_KEY)

    api.search('landscape', page=page_num, results_per_page=5)
    photos_left = api.get_entries()

    # print(api.search('landscape', page=page_num, results_per_page=5))
    # print(api.get_entries())

    api.search('weather', page=page_num, results_per_page=5)
    photos_right = api.get_entries()

    # print(api.search('weather', page=page_num, results_per_page=5))
    # print(api.get_entries())

    photos_list_left = []
    photos_list_right = []
    for photo in photos_left:
        photos_list_left.append({
            'url': photo.medium,
            'description': photo.description,
        })
    for photo in photos_right:
        photos_list_right.append({
            'url': photo.medium,
            'description': photo.description,
        })

    # print(len(photos_list_left + photos_list_right))
    paginator = Paginator(photos_list_left + photos_list_right, 1)
    page_number = page_num
    page_obj = paginator.get_page(page_number)
    # print(page_obj)

    # print(page_obj.has_previous())
    # print(page_obj.previous_page_number())
    #
    # print(page_obj.has_next())
    # print(page_obj.next_page_number())

    context = {
        'photos_list_left': photos_list_left,
        'photos_list_right': photos_list_right,
        'page_obj': page_obj,
    }
    return render(request, 'core/photos.html', context)


def video(request, page_num):
    print(request)
    print(page_num)

    api_key = '563492ad6f917000010000016c169600b01549b190ba2ed39b1d9000'
    py_pexel = PyPexels(api_key=api_key)

    search_videos_top = py_pexel.videos_search(query="forest", page=page_num, per_page=8)
    video_list_top = []

    for video in search_videos_top.entries:
        video_list_top.append({
            'url': video.url,
            'image': video.image,
            'duration': video.duration,
            'user': video.user['name'],
        })
        print(video.url)
        print(video.image)

    search_videos_bottom = py_pexel.videos_search(query="landscape", page=page_num, per_page=8)
    video_list_bottom = []
    for video in search_videos_bottom.entries:
        video_list_bottom.append({
            'url': video.url,
            'image': video.image,
            'duration': video.duration,
            'user': video.user['name'],
        })
        print(video.url)
        print(video.image)

    print(len(video_list_bottom + video_list_top))
    paginator = Paginator(video_list_bottom + video_list_top, 1)
    page_number = page_num
    page_obj = paginator.get_page(page_number)
    print(page_obj)

    context = {
        'video_list_bottom': video_list_bottom,
        'video_list_top': video_list_top,
        'page_obj': page_obj,
    }

    return render(request, 'core/live-cameras.html', context)


def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
            redirect('core:contact')
        else:
            raise ValueError('Validation failed')

    form = ContactForm()
    return render(request, 'core/contact.html', {'contact_form': form})
