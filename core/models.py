from ckeditor.fields import RichTextField
from django.db import models


class Email(models.Model):
    email_address = models.EmailField()

    def __str__(self):
        return self.email_address


class Contact(models.Model):
    your_name = models.CharField(max_length=20)
    your_email_address = models.EmailField()
    your_company_name = models.CharField(max_length=20, null=True, blank=True)
    your_website = models.URLField(null=True, blank=True)
    your_message = RichTextField(max_length=256)

    def __str__(self):
        return self.your_name
