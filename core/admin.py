from django.contrib import admin
from .models import Contact, Email


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    list_display = ('your_name', 'your_email_address')


@admin.register(Email)
class EmailAdmin(admin.ModelAdmin):
    list_display = ('email_address',)
