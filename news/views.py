import requests
from django.core.paginator import Paginator
from django.shortcuts import render
from datetime import datetime


def get_current_time(time):
    formatted_time = ''
    formatted_time += str(time.year) + "-"
    formatted_time += str(time.month) + "-"
    formatted_time += str(time.day - 1)
    return formatted_time


def news(request, name, page_num):
    global url_categories, url_countries, url_hot_news, url_news

    articles_news = []

    # -------------------------- hot_news -------------------------------
    url_hot_news = ('https://newsapi.org/v2/everything?'
                    'qInTitle=+weather forecast&'
                    'from={}&'.format(get_current_time(datetime.today())) +
                    'sortBy=popularity&'
                    'apiKey=21f9eff5ef8e4fa9b470994ce83c2688')

    request_hot_news = requests.get(url_hot_news).json()

    # print(url_hot_news)
    # print(request_hot_news)

    articles_hot_news = []
    for i in range(0, request_hot_news['totalResults'], 1):
        if (request_hot_news['articles'][i]['title'] is not None) and (
                request_hot_news['articles'][i]['description'] is not None) and (
                request_hot_news['articles'][i]['urlToImage'] is not None) and (
                request_hot_news['articles'][i]['content']):
            articles_hot_news.append({
                'title': request_hot_news['articles'][i]['title'],
            })

        if len(articles_hot_news) >= 6:
            break
    # -------------------------- news -------------------------------
    if name == 'none':
        url_news = ('https://newsapi.org/v2/everything?'
                    'qInTitle="weather forecast"&'
                    'from={}&'.format(get_current_time(datetime.today())) +
                    'sortBy=popularity&'
                    'pageSize=7&'
                    'page={}&'.format(page_num) +
                    'apiKey=21f9eff5ef8e4fa9b470994ce83c2688')

        request_news = requests.get(url_news).json()

        # print(url_news)
        # print(request_news)

        for i in range(0, request_news['totalResults'], 1):
            if (request_news['articles'][i]['title'] is not None) and (
                    request_news['articles'][i]['description'] is not None) and (
                    request_news['articles'][i]['urlToImage'] is not None) and (
                    request_news['articles'][i]['content']):
                articles_news.append({
                    'title': request_news['articles'][i]['title'],
                    'description': request_news['articles'][i]['description'],
                    'urlToImage': request_news['articles'][i]['urlToImage'],
                })

            if len(articles_news) >= 5:
                break
    else:
        # -------------------------- categories -------------------------------
        if len(name) > 2:
            url_categories = ('https://newsapi.org/v2/top-headlines?'
                              'category={}&'.format(name) +
                              'pageSize=7&'
                              'page={}&'.format(page_num) +
                              'apiKey=21f9eff5ef8e4fa9b470994ce83c2688')

            request_categories = requests.get(url_categories).json()

            # print(url_categories)
            # print(request_categories)

            for i in range(0, request_categories['totalResults'], 1):
                if (request_categories['articles'][i]['title'] is not None) and (
                        request_categories['articles'][i]['description'] is not None) and (
                        request_categories['articles'][i]['urlToImage'] is not None) and (
                        request_categories['articles'][i]['content']):
                    articles_news.append({
                        'title': request_categories['articles'][i]['title'],
                        'description': request_categories['articles'][i]['description'],
                        'urlToImage': request_categories['articles'][i]['urlToImage'],
                    })

                if len(articles_news) >= 5:
                    break
        # -------------------------- countries -------------------------------
        else:
            url_countries = ('https://newsapi.org/v2/top-headlines?'
                             'country={}&'.format(name) +
                             'pageSize=7&'
                             'page={}&'.format(page_num) +
                             'apiKey=21f9eff5ef8e4fa9b470994ce83c2688')

            request_countries = requests.get(url_countries).json()

            # print(url_countries)
            # print(request_countries)

            for i in range(0, request_countries['totalResults'], 1):
                if (request_countries['articles'][i]['title'] is not None) and (
                        request_countries['articles'][i]['description'] is not None) and (
                        request_countries['articles'][i]['urlToImage'] is not None) and (
                        request_countries['articles'][i]['content']):
                    articles_news.append({
                        'title': request_countries['articles'][i]['title'],
                        'description': request_countries['articles'][i]['description'],
                        'urlToImage': request_countries['articles'][i]['urlToImage'],
                    })

                if len(articles_news) >= 5:
                    break

    # -------------------------- pagination -------------------------------
    # print(len(articles_news))
    paginator = Paginator(articles_news, 1)
    page_number = page_num
    page_obj = paginator.get_page(page_number)
    # print(page_obj)

    # print(page_obj.has_previous())
    # print(page_obj.previous_page_number())

    # print(page_obj.has_next())
    # print(page_obj.next_page_number())

    context = {
        'articles_news': articles_news,
        'articles_hot_news': articles_hot_news,
        'page_obj': page_obj,
        'name': name,
    }

    return render(request, 'news/news.html', context)


def single(request, name):
    # print(request)
    # print(name)
    # -------------------------- hot_news -------------------------------
    articles_hot_news = []
    url_hot_news = ('https://newsapi.org/v2/everything?'
                    'qInTitle=+weather forecast&'
                    'from={}&'.format(get_current_time(datetime.today())) +
                    'sortBy=popularity&'
                    'apiKey=8e8831c2f33b4772a65ea8bb06cf54ff')

    request_hot_news = requests.get(url_hot_news).json()

    # print(url_hot_news)
    # print(request_hot_news)

    for i in range(0, request_hot_news['totalResults'], 1):
        if (request_hot_news['articles'][i]['title'] is not None) and (
                request_hot_news['articles'][i]['description'] is not None) and (
                request_hot_news['articles'][i]['urlToImage'] is not None) and (
                request_hot_news['articles'][i]['content']):
            articles_hot_news.append({
                'title': request_hot_news['articles'][i]['title'],
            })

        if len(articles_hot_news) >= 6:
            break

    # -------------------------- news -------------------------------
    url_news = ('https://newsapi.org/v2/everything?'
                'qInTitle=+{}&'.format(name) +
                'from={}&'.format(get_current_time(datetime.today())) +
                'apiKey=8e8831c2f33b4772a65ea8bb06cf54ff')

    request_news = requests.get(url_news).json()

    # print(url_news)
    # print(request_news)

    context = {
        'title': request_news['articles'][0]['title'],
        'description': request_news['articles'][0]['description'],
        'urlToImage': request_news['articles'][0]['urlToImage'],
        'content': request_news['articles'][0]['content'],
        'articles_hot_news': articles_hot_news,
    }
    return render(request, 'news/single.html', context)
