from django.urls import path
from .views import news, single

app_name = 'news'
urlpatterns = [
    path('news/<str:name>/<int:page_num>/', news, name='news'),
    path('single/<str:name>/', single, name='single'),
]
