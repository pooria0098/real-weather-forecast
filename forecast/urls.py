from django.urls import path
from .views import index

app_name = 'forecast'
urlpatterns = [
    path('', index, name='index'),
]
