from django.contrib import messages
from django.shortcuts import render, redirect
from .forms import SearchCityWeatherForm

from datetime import datetime
import requests


def convert_num_to_textualDescription_windDirection(degree):
    if degree > 337.5:
        return 'North'
    if degree > 292.5:
        return 'North West'
    if degree > 247.5:
        return 'West'
    if degree > 202.5:
        return 'South West'
    if degree > 157.5:
        return 'South'
    if degree > 122.5:
        return 'South East'
    if degree > 67.5:
        return 'East'
    if degree > 22.5:
        return 'North East'


def convert_num_of_day_to_name(day):
    if day == 0:
        return 'Monday'
    if day == 1:
        return 'Tuesday'
    if day == 2:
        return 'Wednesday'
    if day == 3:
        return 'Thursday'
    if day == 4:
        return 'Friday'
    if day == 5:
        return 'Saturday'
    if day == 6:
        return 'Sunday'


def index(request):
    global validCityName
    # print(request)
    # print('-' * 30)
    url = 'http://api.openweathermap.org/data/2.5/forecast?q={}&appid=271d1234d3f497eed5b1d80a07b3fcd1'
    if request.method == 'GET':
        form = SearchCityWeatherForm()
        # print(request.GET)
        # print('-' * 30)
        # for handling weather forecast form
        if 'cityName' in request.GET:
            form = SearchCityWeatherForm(request.GET)
            # print(form)
            # print('-' * 30)
            if form.is_valid():
                validCityName = form.cleaned_data.get('cityName')
            else:
                messages.error(request, 'Validation Error occurred!')
                return redirect('forecast:index')
        # for just rendering templates without fill out form
        else:
            return render(request, 'forecast/index.html', {'search_form': form})

    form = SearchCityWeatherForm()

    # json response in here
    request_climate = requests.get(url.format(validCityName)).json()
    # print(request_climate)
    # print('-' * 30)
    # if response in not success
    if request_climate['cod'] != '200':
        return redirect('forecast:index')

    six_future_days = []
    num_next_day = datetime.today().weekday()
    # print(num_next_day)
    for i in range(8, 39, 8):
        if num_next_day == 6:
            num_next_day = 0
        num_next_day += 1
        next_day = convert_num_of_day_to_name(num_next_day)
        # print(num_next_day)
        six_future_days.append(
            {
                'next_day': next_day,
                'temperature': '{:.0f}'.format(request_climate['list'][i]['main']['temp'] - 273.15),
                'temperature_min': '{:.2f}'.format(request_climate['list'][i]['main']['temp_min'] - 273.15),
                'condition_icon': request_climate['list'][i]['weather'][0]['icon']
            }
        )

    context = {
        'search_form': form,
        # ------- current -------
        'date': datetime.today(),
        'day': convert_num_of_day_to_name(datetime.today().weekday()),
        'location': request_climate['city']['name'],
        'temperature': '{:.0f}'.format(request_climate['list'][0]['main']['temp'] - 273.15),
        'temperature_min': '{:.2f}'.format(request_climate['list'][0]['main']['temp_min'] - 273.15),
        'feels_like': '{:.2f}'.format(request_climate['list'][0]['main']['feels_like'] - 273.15),
        # i gonna use bellow for customizing condition_icon in future
        'condition': request_climate['list'][0]['weather'][0]['main'],
        # ----------------------------------------------
        'condition_description': request_climate['list'][0]['weather'][0]['description'],
        'condition_icon': request_climate['list'][0]['weather'][0]['icon'],
        'speed': '{:.2f}'.format(request_climate['list'][0]['wind']['speed'] * 1000 / 3600),
        'direction': convert_num_to_textualDescription_windDirection(request_climate['list'][0]['wind']['deg']),
        'rain_possibility': request_climate['list'][0]['pop'] * 100,
        # ------- future -------
        'six_future_days': six_future_days,
    }

    return render(request, 'forecast/index.html', context)
