from django.apps import AppConfig


class ForcastConfig(AppConfig):
    name = 'forecast'
