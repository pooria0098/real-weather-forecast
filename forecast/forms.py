from django import forms


class SearchCityWeatherForm(forms.Form):
    cityName = forms.CharField(max_length=20)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['cityName'].widget.attrs.update(
            {
                'placeholder': 'Find your location...',
                'type': 'text',
                'label': '',
            }
        )

